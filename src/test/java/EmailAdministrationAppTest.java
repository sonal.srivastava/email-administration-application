import org.junit.jupiter.api.Test;

public class EmailAdministrationAppTest {
    @Test
    void shouldBeAbleToAssignEmailIDToANewEmployee() {
        Email email = new Email("sonal", "srivastava", Department.DEVELOPMENT, "thoughtworks",
                10, "sonal2497");
    }

    @Test
    void shouldBeAbleToAssignNewEmailIDToAExistingEmployee() {
        Email employeeEmail = new Email("sonal", "srivastava", Department.DEVELOPMENT, "thoughtworks",
                10, "sonal2497");
        employeeEmail.setDepartment(Department.OTHER);
    }

    @Test
    void shouldBeAbleToChangePassword() {
        Email employeeEmail = new Email("sonal", "srivastava", Department.DEVELOPMENT, "thoughtworks",
                10, "sonal2497");
        employeeEmail.setPassword("oamxnsdeifwbsa23oxnm");
    }
}
