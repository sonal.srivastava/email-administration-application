import org.apache.commons.lang3.RandomStringUtils;


public class Email {
    private Department department;
    private final String firstName;
    private final String lastName;
    private final String company;
    private String password;
    private int mailBoxCapacity;
    private final String alternateEmailID;
    private String emailID;

    public Email(String firstName, String lastName, Department department, String company,
                 int mailBoxCapacity, String alternateEmailID) {
        this.firstName=firstName;
        this.lastName=lastName;
        this.department=department;
        this.company=company;
        this.mailBoxCapacity=mailBoxCapacity;
        this.alternateEmailID=alternateEmailID;
        this.emailID = assignEmailID();
        this.password=generatePassword();
    }

    private String assignEmailID() {
        String temp_ = firstName.toLowerCase() + "."+lastName.toLowerCase()+"@"+
                department.getDepartmentName()+"."+company+".com";
        System.out.println("The employee's company email id has been assigned : "+temp_);
        return temp_;
    }

    public void setDepartment(Department department) {
        this.department = department;
        System.out.println("Since your department changed, you're assigned a new company email ID.");
        assignEmailID();
    }

    public void setPassword(String password) {
        this.password = password;
        System.out.println("Your password has been set.");
    }

    public void setMailBoxCapacity(int mailBoxCapacity) {
        this.mailBoxCapacity = mailBoxCapacity;
    }

    private String generatePassword(){
        String generatedPassword = RandomStringUtils.randomAlphanumeric(15);
        System.out.println("Confidential! Please do not share : your password is "+generatedPassword);
        return generatedPassword;
    }
}
