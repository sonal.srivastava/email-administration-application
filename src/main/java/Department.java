public enum Department {
    SALES("sales"),
    DEVELOPMENT("development"),
    ACCOUNTING("accounting"),
    OTHER("other");

    private final String departmentName;

    Department(String departmentName) {
        this.departmentName=departmentName;
    }

    public String getDepartmentName() {
        return departmentName;
    }
}
